# Verlanglijstjes #

Awesome Single Page Application om verlanglijstjes te delen etc.

Dit is een mini project voor INF-I @ de Haagse Hogeschool.

# How to install #

```

cd Verlanglijstjes
npm install
bower install
npm run build
```

Ready to go :)