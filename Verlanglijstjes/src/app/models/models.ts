﻿export class ListItem {
    ListItemId: number;
    Title: string;
    Received: boolean;
    WishList: WishListModel;
}

export class User {
    FullName: string;
    UserName: string;
    Password: string;
    ConfirmPassword: string;
    WishLists: Array<WishListModel>;
}

export class WishListModel {
    Title: String;
    WishListId: Number;
    Items: Array<ListItem>;
    User: User;
    EditMode: Boolean = false;
}
