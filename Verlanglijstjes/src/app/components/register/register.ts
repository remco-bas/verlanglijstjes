import {Component, View, CORE_DIRECTIVES, FORM_DIRECTIVES, Inject} from 'angular2/angular2';
import {Http, HTTP_PROVIDERS, Headers} from 'angular2/http';
import {Router} from 'angular2/router';

import Models = require("../../models/models");
import User = Models.User;

@Component({
    selector: 'my-app-sub',
    viewProviders: [HTTP_PROVIDERS]
})
@View({
    templateUrl: 'src/app/templates/register.html',
    directives: [CORE_DIRECTIVES, FORM_DIRECTIVES]
})
export class Register {
    public newUser: User;
    public response = [];
    private http: Http;
    private _router: Router;

    constructor( @Inject(Http) http: Http, @Inject(Router) _router : Router) {
        this.newUser = new User();
        this.http = http;
        this._router = _router;
    }

    onSubmit() {
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        this.http.post('api/account/register', JSON.stringify(this.newUser),
            {
                headers: headers
            })
            .subscribe(
            res => {
                if (res.status === 200) {
                    this._router.navigate(['/Login', { ref: 'register_succeeded' }]);
                } else {
                    var result = res.json()['ModelState'];

                    console.log(result);
                    this.response = [];
                    for (var a in result) {
                        console.log(result[a][0]);
                        if (result[a][0] && result[a][0] != '')
                            this.response.push(result[a][0]); 
                    }
                }
            });
    }
}