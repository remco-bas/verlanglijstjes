import {Component, View, CORE_DIRECTIVES, FORM_DIRECTIVES, NgClass} from 'angular2/angular2';
import {Inject } from 'angular2/angular2';
import {Http, HTTP_PROVIDERS, Headers} from 'angular2/http';
import Models = require("../../models/models");
import Services = require("../../services/AuthService");
import {Router, RouteParams} from 'angular2/router';

import WishListModel = Models.WishListModel;
import User = Models.User;
import ListItem = Models.ListItem;
import AuthService = Services.AuthService;

@Component({
    selector: 'my-app-sub',
    viewProviders: [HTTP_PROVIDERS, AuthService]
})
@View({
    templateUrl: 'src/app/templates/wishlist.html',
    directives: [CORE_DIRECTIVES, FORM_DIRECTIVES, NgClass]
})
export class Wishlist {
    public wishlists: Array<Models.WishListModel>;
    private http: Http;
    private authService: AuthService;
    private currentUser: User;
    private itemMap: { [key: number]: string; } = {};
    private _routeParams: RouteParams;

    spectateMode: boolean;

    constructor( @Inject(Http) http: Http,
        @Inject(AuthService) authService: AuthService,
        @Inject(RouteParams) _routeParams: RouteParams) {

        this.authService = authService;
        this.http = http;
        this.wishlists = new Array<WishListModel>();
        this._routeParams = _routeParams;

        this.fetchWishlists();
        //TODO: currentUser = this._routeParams.get('id') (?)
        http.get('api/users')
            .map(res => res.json())
            .subscribe(u => this.currentUser = u[0]);
    }

    private fetchWishlists() {
        var headers = this.authService.getAuthHeader();
        var userId = this._routeParams.get('userId');
        this.spectateMode = true;

        if (!userId || userId == '') {
            userId = this.authService.getUser().id;
            this.spectateMode = false;
        }

        this.http.get('api/WishLists?userId=' + userId, {
            headers: headers
        })
            .map(res => res.json())
            .subscribe(w => this.wishlists = w);
    }

    newItem(wishList: WishListModel) {
        var listId = wishList.WishListId.valueOf();
        var newItemText = this.itemMap[listId];

        var item = new ListItem();
        item.Title = newItemText;
        item.WishList = wishList;
        item.Received = false;

        var headers = this.authService.getAuthHeader();
        headers.append('Content-Type', 'application/json');
        this.http.post('api/ListItems', JSON.stringify(item), {
            headers: headers
        }).subscribe(res => {
            this.itemMap[listId] = '';
            this.fetchWishlists();
        });
    }

    onItemGet(item: ListItem) {
        item.Received = !item.Received;
        this.updateItem(item);
    }

    updateItem(item: ListItem) {
        var headers = this.authService.getAuthHeader();
        headers.append('Content-Type', 'application/json');
        this.http.put('api/ListItems/' + item.ListItemId,
            JSON.stringify(item), {
                headers: headers
            }).subscribe(res => {
                //this.fetchWishlists();
            });
    }

    deleteItem(item: ListItem) {
        var id = item.ListItemId;

        var headers = this.authService.getAuthHeader();
        this.http.delete('api/ListItems/' + id, {
            headers: headers
        }).subscribe(res => {
            if (res.status != 200) {
                console.error(res);
            }

            this.fetchWishlists();
        });
    }

    editWishList(wl: WishListModel) {
        wl.EditMode = true;
    }

    updateWishList(wl: WishListModel) {

        var headers = this.authService.getAuthHeader();

        var id = wl.WishListId;

        headers.append('Content-Type', 'application/json');
        this.http.put('api/WishLists/' + id, JSON.stringify(wl),
            {
                headers: headers
            })
            .subscribe(res => {
                this.fetchWishlists();
                wl.EditMode = false;
            });

    }

    newWishList() {
        var w = new WishListModel();
        w.Title = "--Nieuw verlanglijstje--";
        this.wishlists.push(w);

        this.persistNewWishList(w);
    }

    private persistNewWishList(w: WishListModel) {

        var headers = this.authService.getAuthHeader();

        headers.append('Content-Type', 'application/json');
        this.http.post('api/WishLists', JSON.stringify(w),
            {
                headers: headers
            })
            .subscribe(res => this.fetchWishlists());

    }
}
