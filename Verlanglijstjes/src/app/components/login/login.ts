﻿import {Component, View, CORE_DIRECTIVES,
FORM_DIRECTIVES, Inject, bootstrap} from 'angular2/angular2';
import {Http, HTTP_PROVIDERS, Headers} from 'angular2/http';
import Services = require("../../services/AuthService");

import AuthService = Services.AuthService;

@Component({
    selector: 'my-app-sub',
    viewProviders: [HTTP_PROVIDERS, AuthService]
})
@View({
    templateUrl: 'src/app/templates/login.html',
    directives: [CORE_DIRECTIVES, FORM_DIRECTIVES]
})
export class Login {
    private userName: string;
    private password: string;
    private authService: AuthService;

    constructor( @Inject(AuthService) auth: AuthService) {
        this.authService = auth;
    }

    onLogin() {
        this.authService.authenticate(this.userName, this.password,
        (res) => {
            if (res.statusCode === 200) {
                // redirect to front page
            }
        });
    }
}
