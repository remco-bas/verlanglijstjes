import {Component, View, CORE_DIRECTIVES} from 'angular2/angular2';
import {Inject} from 'angular2/angular2';
import {Http, HTTP_PROVIDERS, Headers} from 'angular2/http';
import Models = require("../../models/models");
import User = Models.User;

import {Router} from 'angular2/router';

@Component({
    selector: 'my-app-sub',
    viewProviders: [HTTP_PROVIDERS]
  
})
@View({
        templateUrl: 'src/app/templates/people.html',
        directives:[CORE_DIRECTIVES]
})
      
export class People {
    public users: Array<User>;
    private _router: Router;

    constructor( @Inject(Http) Http: Http, @Inject(Router) _router : Router) {
        this.users = new Array<User>();

        Http.get('api/users')
            .map(res => res.json())
            .subscribe(u => this.users = u);
        this._router = _router;
    }
	
	onSelect(userId : string){
		this._router.navigate(['/Wishlist', {userId : userId}])
	}
}

