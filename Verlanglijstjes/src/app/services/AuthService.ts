import {Inject, Injectable, provide} from 'angular2/angular2';
import {Http, HTTP_PROVIDERS, Headers} from 'angular2/http';
import {Router} from 'angular2/router';

export class AuthService {

    private http: Http;
    private router: Router;
    private sKey: string = "token";
    private sExpireKey: string = "expires_in";

    constructor( @Inject(Http) http: Http,
        @Inject(Router) router: Router) {
        this.http = http;
        this.router = router;
    }

    public authenticate(userName: string, password: string, callback: Function) {
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');

        var requestBody = 'username=' + userName + '&password=' + password
            + '&grant_type=password';

        this.http.post('/token',
            requestBody, {
                headers: headers
            })
            .subscribe(res => {
                var data = res.json();
                this.setToken(data.access_token);
                this.setExpireDate(this.calculateExpireDate(data.expires_in));

                console.log(this.getExpireDate());

                if (res.status === 200) {
                    this.fetchUserDetails(userName, callback);
                }
            });
    }

    private fetchUserDetails(username: string, callback: Function) {
        this.http.get('api/users?username=' + username)
            .subscribe(res => {
            var data = res.json();
                var user = {
                    fullName: data.FullName,
                    id: data.Id
                };
                this.setUser(user);
                this.router.navigate(['/Wishlist']);
            });
    }

    public logout() {
        localStorage.removeItem(this.sKey);
        localStorage.removeItem(this.sExpireKey);
        localStorage.removeItem('userN');

        this.navigateLoginPage();
    }

    public getAuthHeader() {
        if (!this.isAuthenticated()) {
            this.navigateLoginPage();
        }

        var headers = new Headers();
        headers.append('Authorization',
            'bearer ' + this.getToken());

        return headers;
    }

    /**
     * Returns true if authenticated
     * @returns {boolean} 
     */
    public isAuthenticated() {
        // false if no token is set
        if (!this.getToken()) {
            return false;
        }
        // false if expire date <= now
        if (this.getExpireDate().getTime() <= new Date().getTime()) {
            return false;
        }

        return true;
    }

    private navigateLoginPage() {
        this.router.navigate(['/Login']);
    }

    /**
     * Caluclates and returns expiration date of session
     * @param timeLeftSeconds time left in seconds before session expires (expires_in parameter by owin)
     * @returns {Date} date containing expire date 
     */
    private calculateExpireDate(timeLeftSeconds: number) {
        var currentDate = new Date().getTime();
        return new Date(currentDate + timeLeftSeconds * 1000);
    }

    private getExpireDate() {
        var ms = localStorage.getItem(this.sExpireKey);
        var milliseconds = new Number(ms);

        return new Date(milliseconds.valueOf());
    }

    private setExpireDate(expireDate: Date) {
        localStorage.setItem(this.sExpireKey, expireDate.getTime().toString());
    }

    private setToken(token: string) {
        localStorage.setItem(this.sKey, token);
    }

    private getToken() {
        return localStorage.getItem(this.sKey);
    }

    private setUser(username) {
        localStorage.setItem('userN', JSON.stringify(username));
    }

    public getUser() {
        if (this.isAuthenticated) {
            try {
                return JSON.parse(localStorage.getItem('userN'));
            } catch (e) {
                console.warn(e);
            }
        }
    }
}
