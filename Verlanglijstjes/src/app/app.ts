import {Component, View, bootstrap, bind, Inject, CORE_DIRECTIVES} from 'angular2/angular2';
import {ROUTER_PROVIDERS, RouterOutlet, RouteConfig, RouterLink} from 'angular2/router'
import Service = require("./services/AuthService");
import {Http, HTTP_PROVIDERS, Headers} from 'angular2/http';

import { Register } from './components/register/register';
import { Wishlist } from './components/wishlist/wishlist';
import { People } from './components/people/people';
import { Login } from "./components/login/login";
import AuthService = Service.AuthService;

@Component({
    selector: 'my-app',
    viewProviders: [AuthService, HTTP_PROVIDERS]
})
@View({
    templateUrl: 'src/app/templates/app.html',
    directives: [RouterOutlet, RouterLink, CORE_DIRECTIVES]
})
@RouteConfig([
    { path: '/register', component: Register, as: 'Register' },
    { path: '/login', component: Login, as: 'Login' },
    { path: '/people', component: People, as: 'People' },
    { path: '/wishlist', component: Wishlist, as: 'Wishlist' }
])


class AppComponent {

    private authService: AuthService;

    constructor( @Inject(AuthService) authService) {
        this.authService = authService;
    }

    public isAuthenticated() {
        return this.authService.isAuthenticated();
    }

    public logout() {
        this.authService.logout();
    }

    public getUserName() {
        var user = this.authService.getUser();
        if (!!user) return user.fullName;
    }
}

bootstrap(AppComponent, [ROUTER_PROVIDERS]);