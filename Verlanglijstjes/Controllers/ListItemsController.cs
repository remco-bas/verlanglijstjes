﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Verlanglijstjes.Models;

namespace Verlanglijstjes.Controllers
{
    [Authorize]
    public class ListItemsController : ApiController
    {
        private AuthContext db = new AuthContext();

        // GET: api/ListItems
        public IQueryable<ListItem> GetItems(int wishListId)
        {
            return db.Items.Where(x => x.WishList.WishListId == wishListId);
        }

        // GET: api/ListItems/5
        [ResponseType(typeof(ListItem))]
        public async Task<IHttpActionResult> GetListItem(int id)
        {
            ListItem listItem = await db.Items.FindAsync(id);
            if (listItem == null)
            {
                return NotFound();
            }

            return Ok(listItem);
        }

        // PUT: api/ListItems/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutListItem(int id, ListItem listItem)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != listItem.ListItemId)
            {
                return BadRequest();
            }

            db.Entry(listItem).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ListItemExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ListItems
        [ResponseType(typeof(ListItem))]
        public async Task<IHttpActionResult> PostListItem(ListItem listItem)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var wishList = await db.WishLists.SingleOrDefaultAsync(l => l.WishListId == listItem.WishList.WishListId);
            listItem.WishList = wishList;

            db.Items.Add(listItem);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = listItem.ListItemId }, listItem);
        }

        // DELETE: api/ListItems/5
        [ResponseType(typeof(ListItem))]
        public async Task<IHttpActionResult> DeleteListItem(int id)
        {
            ListItem listItem = await db.Items.FindAsync(id);
            if (listItem == null)
            {
                return NotFound();
            }

            db.Items.Remove(listItem);
            await db.SaveChangesAsync();

            return Ok(listItem);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ListItemExists(int id)
        {
            return db.Items.Count(e => e.ListItemId == id) > 0;
        }
    }
}