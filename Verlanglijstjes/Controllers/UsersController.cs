﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Verlanglijstjes.Models;

namespace Verlanglijstjes.Controllers
{
    public class UsersController : ApiController
    {
        private AuthContext db = new AuthContext();

        public User GetUser(string username)
        {
            return db.Users.Single(u => u.UserName.Equals(username));
        }

        // GET: api/Users
        public IQueryable<object> GetUsers()
        {
            return db.Users.Select(x => new
            {
                x.FullName,
                x.Id,
                WishLists = x.WishLists.Select(w => new
                {
                    w.WishListId,
                    w.Title,
                    User = new
                    {
                        x.UserName,
                        x.FullName,
                        x.Id
                    },
                    Items = w.Items.Select(i => new
                    {
                        i.ListItemId,
                        i.Received,
                        i.Title
                    })
                }),
                x.UserName
            });
        }

        [Authorize]
        public async Task<IHttpActionResult> Put(string id, User user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            var found = await db.Users.SingleOrDefaultAsync(x => x.Id.Equals(id));
            found.FullName = user.FullName;
            found.WishLists = user.WishLists;
            found.Password = user.Password;
            found.ConfirmPassword = user.ConfirmPassword;

            db.Entry(found).State = EntityState.Modified;
            await db.SaveChangesAsync();

            return Ok();
        }

        [Authorize]
        public async Task<IHttpActionResult> Delete()
        {
            string id = User.Identity.GetUserId();
            User user;
            try
            {
                user = await db.Users.SingleAsync(x => x.Id.Equals(id));
            }
            catch (InvalidOperationException)
            {
                return NotFound();
            }


            db.Entry(user).State = EntityState.Deleted;
            await db.SaveChangesAsync();

            return Ok();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}