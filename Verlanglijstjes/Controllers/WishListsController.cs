﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Verlanglijstjes.Models;

namespace Verlanglijstjes.Controllers
{
    [Authorize]
    public class WishListsController : ApiController
    {
        private AuthContext db = new AuthContext();

        // GET: api/WishLists
        public IQueryable<object> GetWishLists(string userId)
        {
            //return db.WishLists.Where(x => x.User.Id == userId);
            var list = from w in db.WishLists
                       select new
                       {
                           w.WishListId,
                           w.Title,
                           User = new
                           {
                               w.User.UserName,
                               w.User.FullName,
                               w.User.Id
                           },
                           Items = w.Items.Select(i => new
                           {
                               i.ListItemId,
                               i.Received,
                               i.Title
                           })
                       };
            return list.Where(x => x.User.Id == userId);
        }

        // GET: api/WishLists/5
        [ResponseType(typeof(WishList))]
        public async Task<IHttpActionResult> GetWishList(int id)
        {
            WishList wishList = await db.WishLists.FindAsync(id);
            if (wishList == null)
            {
                return NotFound();
            }

            return Ok(wishList);
        }

        // PUT: api/WishLists/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutWishList(int id, WishList wishList)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != wishList.WishListId)
            {
                return BadRequest();
            }

            db.Entry(wishList).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!WishListExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/WishLists
        [Authorize]
        [ResponseType(typeof(WishList))]
        public async Task<IHttpActionResult> PostWishList(WishList wishList)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var name = User.Identity.Name;
            var user = await db.Users.SingleAsync(u => u.UserName.Equals(name));

            wishList.User = user;
            db.WishLists.Add(wishList);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = wishList.WishListId }, wishList);
        }

        // DELETE: api/WishLists/5
        [ResponseType(typeof(WishList))]
        public async Task<IHttpActionResult> DeleteWishList(int id)
        {
            WishList wishList = await db.WishLists.FindAsync(id);
            if (wishList == null)
            {
                return NotFound();
            }

            // first delete items
            await DeleteItemsOfWishList(wishList);

            wishList = await db.WishLists.FindAsync(id);
            db.WishLists.Remove(wishList);
            await db.SaveChangesAsync();

            return Ok(wishList);
        }

        private async Task DeleteItemsOfWishList(WishList wishList)
        {
            var items = wishList.Items;
            for (int i = 0; i < wishList.Items.Count; i++)
            {
                db.Items.Remove(items[i]);
            }
            await db.SaveChangesAsync();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool WishListExists(int id)
        {
            return db.WishLists.Count(e => e.WishListId == id) > 0;
        }
    }
}