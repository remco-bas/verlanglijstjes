using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Verlanglijstjes.Models
{
    public class User : IdentityUser
    {
//        [Key]
//        public int UserId { get; set; }
        // Linked wish lists
        public virtual IList<WishList> WishLists { get; set; }
        
        public string FullName { get; set; }
        
        // Data attributes for Owin authentication
        [Required]
        [Display(Name = "User name")]
        public override string UserName { get; set; }
        
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        [NotMapped]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [NotMapped]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

}
