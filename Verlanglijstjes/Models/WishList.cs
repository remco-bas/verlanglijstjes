using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Verlanglijstjes.Models
{
    public class WishList
    {
        [Key]
        public int WishListId { get; set; }
        [Required]
        public string Title { get; set; }
        public virtual IList<ListItem> Items { get; set; }
		public virtual User User { get; set; }
    }
}