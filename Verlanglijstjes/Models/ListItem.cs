using System.ComponentModel.DataAnnotations;
using System.Web.Script.Serialization;
using Newtonsoft.Json;

namespace Verlanglijstjes.Models
{
    public class ListItem
    {
        [Key]
        [JsonProperty]
        public int ListItemId { get; set; }
        [MinLength(1), MaxLength(100)]
        [JsonProperty]
        public string Title { get; set; }
        [JsonProperty]
        public bool Received { get; set; }

        [ScriptIgnore]
        public virtual WishList WishList { get; set; }
    }
}
