﻿using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Verlanglijstjes.Models
{
    public class AuthContext : IdentityDbContext<User>
    {
        public AuthContext()
            : base("name=WishListDB2")
        {
            Database.SetInitializer<AuthContext>(new CreateDatabaseIfNotExists<AuthContext>());
        }
        
        public DbSet<WishList> WishLists { get; set; }
        public DbSet<ListItem> Items { get; set; }
    }
}
